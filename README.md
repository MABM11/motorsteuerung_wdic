# Motorsteuerung_Wdic

## Introduction

The project involves sending and receiving random data via WLAN, which drives a simple DC motor. The DC motor has a supply of 9V, as a lower voltage would cause the whole project not to work. The transmission data is sent via a sending routine every 5s. https://randomnerdtutorials.com/esp32-esp-now-encrypted-messages/ 

## Project members
- Marian Biedermann
- Lukas Gsodam

## Contruction
![Construction](construction.jpeg)

## Layout
![Layout](Layout.png)

## Librarys
<details><summary>Transimission</summary>
- esp_now.h <br>
- Wifi.h
</details>


<details><summary>Receiver</summary>
- esp_now.h <br>
- Wifi.h
</details>
